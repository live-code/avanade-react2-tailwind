import './App.css'
import { BrowserRouter, NavLink, Route, Routes } from 'react-router-dom';
import HomePage from './pages/home/HomePage';
import ProductsPage from './pages/products/ProductsPage';

function App() {

  return (
    <BrowserRouter>
      <NavLink to="" className="btn danger">home</NavLink>
      <NavLink to="products" className="btn danger">catalog</NavLink>

      <br/>
      <Routes>
        <Route path="" element={<HomePage />} />
        <Route path="products" element={<ProductsPage />} />
      </Routes>

    </BrowserRouter>
  )
}

export default App
