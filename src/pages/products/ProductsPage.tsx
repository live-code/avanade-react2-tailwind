import { ProductsForm } from './components/ProductsForm';
import { ProductsList } from './components/ProductsList';
import { useProducts } from './hooks/useProducts';

export default function ProductsPage() {

  const {
    activeProduct, products, isModalOpened,
    actions
  } = useProducts();

  return <div>

    <i className="fa fa-plus-circle fa-2x" onClick={actions.openModal}></i>

    {
      isModalOpened &&
        <ProductsForm
          activeItem={activeProduct}
          onSave={actions.save}
          onReset={actions.reset}
          onClose={actions.closeModal}
        />
    }

    <ProductsList
      activeItem={activeProduct}
      items={products}
      onDelete={actions.deleteProduct}
      onItemSelect={actions.selectItem}
    />

  </div>
}
