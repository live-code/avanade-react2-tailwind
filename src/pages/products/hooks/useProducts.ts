import axios from 'axios';
import { useEffect, useState } from 'react';
import { Product } from '../../../model/product';
import { useModal } from './useModal';

const initialState = { name: '', cost: 0};

export function useProducts() {
  const [products, setProducts] = useState<Product[]>([]);
  const [activeProduct, setActiveProduct] = useState<Partial<Product>>(initialState)
  const { isModalOpened, openModal, closeModal} = useModal();

  useEffect(() => {
    axios.get<Product[]>('http://localhost:3000/products')
      .then(res => {
        setProducts(res.data)
      })
  }, [])

  function deleteProduct(id: number) {
    axios.delete(`http://localhost:3000/products/${id}`)
      .then(res => {
        setProducts(
          prevState => prevState.filter(p => p.id !== id)
        )
      })
  }

  function saveHandler(localFormData: Partial<Product>) {
    if (localFormData.id) {
      edit(localFormData);
    } else {
      add(localFormData)
    }
    resetHandler();
    closeModal();
  }

  function add(localFormData: Partial<Product>) {
    axios.post<Product>('http://localhost:3000/products', localFormData)
      .then(res => {
        setProducts(prevState => [...prevState, res.data])
      })
  }

  function edit(localFormData: Partial<Product>) {
    axios.patch<Product>(`http://localhost:3000/products/${localFormData?.id}`, localFormData)
      .then(res => {
        setProducts(
          prevState => prevState.map(p => {
            return p.id === localFormData.id ? res.data : p
          })
        )
      })
  }

  function selectItemHandler(product: Product) {
    setActiveProduct(product);
    resetHandler();
    openModal()
  }

  function resetHandler() {
    setActiveProduct(initialState)
  }


  return {
    activeProduct,
    products,
    isModalOpened,

    actions: {
      openModal,
      closeModal,

      reset: resetHandler,
      selectItem: selectItemHandler,
      save: saveHandler,
      deleteProduct,
    }
  }
}
