import { useState } from 'react';

export function useModal() {
  const [isModalOpened, setIsModalOpened] = useState(false)

  function openModal() {
    setIsModalOpened(true);
  }

  function closeModal() {
    setIsModalOpened(false);
  }

  return {
    isModalOpened,
    openModal,
    closeModal
  }
}
