import clsx from 'clsx';
import { Product } from '../../../model/product';
import { ProductsListItem } from './ProductsListItem';
import { ProductsListItemOfferta } from './ProductsListItemOfferta';

interface ProductsListProps {
  items: Product[];
  activeItem: Partial<Product>;
  onDelete: (id: number) => void;
  onItemSelect: (p: Product) => void;
}
export function ProductsList(props: ProductsListProps) {

  return <div>

    {
      props.items.map(p => {
        return p.cost < 10 ?
          <ProductsListItemOfferta
            key={p.id}
            product={p}
            selected={p.id === props.activeItem?.id }
            onDelete={props.onDelete}
            onItemSelect={props.onItemSelect}
          /> :
          <ProductsListItem
            key={p.id}
            product={p}
            selected={p.id === props.activeItem?.id }
            onDelete={props.onDelete}
            onItemSelect={props.onItemSelect}
          />
      })
    }

  </div>
}
