import { ChangeEvent, FormEvent, useEffect, useState } from 'react';
import { Product } from '../../../model/product';

interface ProductsFormProps {
  activeItem: Partial<Product>;
  onClose: () => void;
  onReset: () => void;
  onSave: (localFormData: Partial<Product>) => void;
}


export function ProductsForm(props: ProductsFormProps) {
  const [formData, setFormData] = useState<Partial<Product>>(props.activeItem)

  useEffect(() => {
    setFormData(props.activeItem)
  }, [props.activeItem])

  function saveHandler(e: FormEvent<HTMLFormElement>) {
    e.preventDefault()
    props.onSave(formData)
  }

  function changeHandler(e: ChangeEvent<HTMLInputElement>) {
    const name = e.currentTarget.name;
    const value = e.currentTarget.value;
    const type = e.currentTarget.type;
    setFormData({...formData, [name]: type === 'number' ? +value : value })
  }

  const isNameValid = formData.name && formData.name.length > 1;
  const isCostValid = formData.cost && formData.cost > 0;
  const isValid =  isNameValid && isCostValid;

  return (
    <div className="fixed bg-gray-200 top-0 bottom-0 w-full">
      <form
        onSubmit={saveHandler}
        className="w-96 mx-auto"
      >

        <i className="fa fa-times" onClick={props.onClose}></i>
        <hr/>

        <input type="text" placeholder="product name" name="name"
               value={formData.name} onChange={changeHandler}/>
        <input type="number" placeholder="product cost" name="cost"
               value={formData.cost} onChange={changeHandler}/>

        <hr/>
        <button type="submit" disabled={!isValid} className="btn accent">
          {formData?.id ? 'EDIT' : 'ADD'}
        </button>
        <button type="button" onClick={props.onReset}>RESET</button>
      </form>

    </div>
  )
}
