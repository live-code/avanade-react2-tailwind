import clsx from 'clsx';
import React, { useState } from 'react';
import { Product } from '../../../model/product';

interface ProductsListItemProps {
  product: Product;
  selected: boolean;
  onDelete: (id: number) => void;
  onItemSelect: (p: Product) => void;
}
export function ProductsListItem(props: ProductsListItemProps) {
  const {product: p} = props;
  const [open, setOpen] = useState(false)

  function deleteHandler(e: React.MouseEvent) {
    e.stopPropagation();
    props.onDelete(p.id)
  }

  return (
    <div className={clsx('p-3 border-b', {'bg-sky-500': props.selected})}>
      <div
        onClick={() => props.onItemSelect(p)}
        className="flex justify-between items-center"
      >
        <div>
          {p.name}
        </div>
        <div className="flex gap-3 items-center">
          € {p.cost}
          <i className="fa fa-trash"
             onClick={deleteHandler}></i>

          <i className="fa fa-arrow-down"
             onClick={(e) => {
               e.stopPropagation();
               setOpen(s => !s)
             }}></i>
        </div>

      </div>
      {
       open && <div>
          contenuto extra
        </div>
      }
    </div>

  )
}
